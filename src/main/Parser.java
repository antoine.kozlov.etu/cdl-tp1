package main;

import org.json.JSONObject;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

enum ParserType {
    JSON(".json"),
    INI(".ini");
    final String extension;
    private ParserType(String extension) {
        this.extension = extension;
    }

    public static ParserType getTypeFromExtension(String extension) {
        for(ParserType tmp: ParserType.values()) {
            if(tmp.extension.equals(extension)) return tmp;
        }
        return null;
    }
}

public class Parser {

    public Map<String,Object> parse(String content, ParserType type) {
        try {
            return switch (type) {
                case JSON -> fromJson(content);
                case INI -> fromIni(content);
                default -> throw new RuntimeException("Not implemented");
            };
        } catch(Exception e) {
            System.err.println("Error while parsing file");
            System.exit(1);
        }
        return null;
    }

    private Map<String,Object> fromIni(String content) throws IOException {
        Properties properties = new Properties();
        properties.load(new StringReader(content));
        Map<String, Object> map = new HashMap<>();
        for(Object k: properties.keySet()) {
            String key = (String) k;
            map.put(key, properties.get(k));
        }
        return map;
    }

    private Map<String,Object> fromJson(String content) {
        return  new JSONObject(content).toMap();
    }

    public void display(Map<String,Object> params) {
        for (String key : params.keySet()) {
            System.out.println(key + ": " + params.get(key));
        }
    }
}
