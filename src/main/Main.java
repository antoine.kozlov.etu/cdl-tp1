package main;

import org.apache.commons.cli.*;
import org.json.JSONObject;

import java.io.*;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

public class Main {

    static final String DEFAULT_CONFIG_PATH = "C:\\Users\\Kozlov\\Documents\\config.json";

    public static void main(String[] args) {
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        options.addOption(null, "configuration", true, "Fichier de configuration");
        CommandLine cmdLine = null;
        try {
            cmdLine = parser.parse(options, args);
        } catch (ParseException exp) {
            System.err.println("Unexpected exception:" + exp.getMessage());
            System.exit(1);
        }
        String path = cmdLine.getOptionValue("configuration");
        if(path == null){
            path = System.getenv("CDL_CONF");
            if(path == null) {
                path = DEFAULT_CONFIG_PATH;
            }
        }
        File configurationFile = new File(path);
        if(!configurationFile.exists()) {
            System.err.println("Configuration file : " + configurationFile.getAbsolutePath() + " does not exist.");
            System.exit(1);
        }

        String content = getContentFile(configurationFile);
        String extension = getExtension(configurationFile.getName());
        Parser configParser = new Parser();
        Map<String, Object> params = configParser.parse(content, ParserType.getTypeFromExtension(extension));
        configParser.display(params);

    }

    static String getExtension(String filename) {
        if(!filename.contains(".")) return null;
        int idx = filename.lastIndexOf(".");
        return filename.substring(idx);
    }

    static String getContentFile(File file) {
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
